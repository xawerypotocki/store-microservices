package com.example.product.jms;


import com.example.common.model.ProductDetails;
import com.example.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.util.HashSet;

@Component
public class MsgListener implements MessageListener {

    @Autowired
    private ProductService productService;

    @Override
    public void onMessage(Message message) {

        if (message instanceof ObjectMessage) {
            ObjectMessage objectMessage = (ObjectMessage) message;
            Object o = null;
            try {
                o = objectMessage.getObject();
            } catch (JMSException e) {
                e.printStackTrace();
            }
            if (o instanceof HashSet) {
                HashSet<ProductDetails> products = (HashSet) o;
                System.out.println("Received products: ");
                productService.replyWithProductDetails(products);

            }
        } else {
            System.out.println("Received: " + message);
        }
    }
}
