package com.example.product.controller;

import com.example.product.entity.Product;
import com.example.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping("/products")
    public List<Product> getAllProducts() {
        return productService.getAllValidProducts();
    }

    @PostMapping("/products")
    public ResponseEntity<String> addProduct(@RequestBody Product product) {
        String message;
        try {
            validateProduct(product);
            UUID id = productService.addProduct(product);
            message = id.toString();
        }
        catch (Exception e) {
            message = e.toString();
        }
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    private void validateProduct(Product product) {
        if (product.getPrice() == null || product.getName() == null || product.getValidTo() == null || product.getValidFrom() == null) {
            throw new IllegalArgumentException("Price, name and dates can not be empty.");
        }
    }
}
