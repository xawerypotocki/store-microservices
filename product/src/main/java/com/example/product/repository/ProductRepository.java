package com.example.product.repository;

import com.example.product.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;


public interface ProductRepository extends CrudRepository<Product, UUID> {
    List<Product> findByValidFromBeforeAndValidToAfter(Date dateFrom, Date dateTo);
    Set<Product> findAllById(Iterable<UUID> id);
}
