package com.example.product.service;

import com.example.product.entity.Product;
import com.example.product.jms.Transmitter;
import com.example.common.model.ProductDetails;
import com.example.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final Transmitter<HashSet<ProductDetails>> transmitter;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, Transmitter<HashSet<ProductDetails>> transmitter) {
        this.productRepository = productRepository;
        this.transmitter = transmitter;
    }


    @Override
    public List<Product> getAllValidProducts() {
        Date currentDate = new Date();
        return productRepository.findByValidFromBeforeAndValidToAfter(currentDate, currentDate);
    }

    @Override
    public UUID addProduct(Product product) {
        UUID id = UUID.randomUUID();
        product.setId(id);
        productRepository.save(product);
        return id;
    }

    @Override
    public void replyWithProductDetails(Set<ProductDetails> productDetails) {
        Set<Product> products = productRepository.findAllById(productDetails.stream().map(ProductDetails::getId).collect(Collectors.toSet()));
        Map<UUID, String> productsMap = products.stream().collect(Collectors.toMap(Product::getId, Product::getName));
        productDetails.stream().forEach(p -> p.name = productsMap.get(p.id));

        if (productDetails instanceof HashSet) {
            transmitter.sendMessage((HashSet) productDetails);
        }
    }
}
