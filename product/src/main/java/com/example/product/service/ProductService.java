package com.example.product.service;

import com.example.product.entity.Product;
import com.example.common.model.ProductDetails;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface ProductService {

    List<Product> getAllValidProducts();

    UUID addProduct(Product product);

    void replyWithProductDetails(Set<ProductDetails> products);
}
