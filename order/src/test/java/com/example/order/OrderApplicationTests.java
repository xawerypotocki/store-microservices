package com.example.order;

import com.example.common.model.OrderDetails;
import com.example.common.model.ProductDetails;
import com.example.order.entity.Order;
import com.example.order.jms.Transmitter;
import com.example.order.repository.OrderRepository;
import com.example.order.service.OrderServiceImpl;
import com.example.order.service.ProductDetailsService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
class OrderApplicationTests {

	@Autowired
	ProductDetailsService productDetailsService;

	@Mock
	ProductDetailsService productDetailsServiceMock;

	@Mock
	OrderRepository repositoryMock;

	@Mock
	Transmitter<HashSet<ProductDetails>> transmitterMock;

	@InjectMocks
	OrderServiceImpl orderServiceImpl;

	@Test
	public void testGetOrderDetails() {
		//GIVEN
		UUID testOrderId = UUID.randomUUID();
		Order testOrder = new Order();
		testOrder.setId(testOrderId);
		testOrder.setProducts(new HashSet<>());
		Optional<Order> testOrderOpt = Optional.of(testOrder);

		UUID testProductId0 = UUID.randomUUID();
		String testName0 = "Test name of product 0";
		UUID testProductId1 = UUID.randomUUID();
		String testName1 = "Test name of product 1";
		ProductDetails testProduct0 = new ProductDetails(testProductId0);
		ProductDetails testProduct1 = new ProductDetails(testProductId1);
		testProduct0.name = testName0;
		testProduct1.name = testName1;
		testOrder.getProducts().add(testProductId0);
		testOrder.getProducts().add(testProductId1);


		//WHEN
		when(repositoryMock.findById(Mockito.any())).thenReturn(testOrderOpt);
		when(productDetailsServiceMock.get(eq(testProductId0), anyLong(), any())).thenReturn(testProduct0);
		when(productDetailsServiceMock.get(eq(testProductId1), anyLong(), any())).thenReturn(testProduct1);
		doNothing().when(transmitterMock).sendMessage(any());
		OrderDetails order = orderServiceImpl.getOrderWithDetails(testOrderId);

		//THEN
		Map<UUID, ProductDetails> orderMap = order.products.stream().collect(Collectors.toMap(o -> o.id, o -> o));
		assertEquals(testName0, orderMap.get(testProductId0).getName());
		assertEquals(testName1, orderMap.get(testProductId1).getName());
	}


	@Test
	public void testGetProductDetails() {
		//GIVEN
		UUID testProductId0 = UUID.randomUUID();
		String testName0 = "Test name of product 0";
		UUID testProductId1 = UUID.randomUUID();
		String testName1 = "Test name of product 1";
		ProductDetails testProduct0 = new ProductDetails(testProductId0);
		ProductDetails testProduct1 = new ProductDetails(testProductId1);
		testProduct0.name = testName0;
		testProduct1.name = testName1;

		Set<ProductDetails> products = new HashSet<>();
		products.add(testProduct0);
		products.add(testProduct1);


		//WHEN
		new Thread(() -> {
			Object monitor = new Object();

			synchronized (monitor) {
				try {
					monitor.wait(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			productDetailsService.add(products);
		}).run();

		ProductDetails product0 = productDetailsService.get(testProductId0, 2000, this);
		ProductDetails product1 = productDetailsService.get(testProductId1, 2000, this);

		//THEN
		assertEquals(testName0, product0.name);
		assertEquals(testName1, product1.name);
	}
}
