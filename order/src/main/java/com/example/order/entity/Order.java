package com.example.order.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="orders")
public class Order {
    @Id
    private UUID id;

    @Column(name = "date")
    private Date date;

    @ElementCollection
    @CollectionTable(name = "ordered_products", joinColumns = @JoinColumn(name = "order_id"))
    @Column(name = "product_id") // 3
    private Set<UUID> products;

    @Column(name = "total_qty")
    private Integer totalQty;

    @Column(name = "total_price")
    private Float totalPrice;

    @Column(name = "customer_id")
    private UUID customerId;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<UUID> getProducts() {
        return products;
    }

    public void setProducts(Set<UUID> products) {
        this.products = products;
    }

    public Integer getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(Integer totalQty) {
        this.totalQty = totalQty;
    }

    public Float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }
}
