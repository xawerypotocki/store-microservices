package com.example.order.controller;

import com.example.order.entity.Order;
import com.example.order.jms.Transmitter;
import com.example.common.model.OrderDetails;
import com.example.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.UUID;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private Transmitter transmitter;

    @GetMapping("/orders")
    public Set<Order> getAllOrders() {
        return orderService.getAllOrders();
    }

    @GetMapping("/orders/{id}")
    public OrderDetails getOrderWithDetails(@PathVariable String id) {
        return orderService.getOrderWithDetails(UUID.fromString(id));
    }

//    @PostMapping("/send")
//    public void getSend() {
//
//        System.out.println("TEEEST SENDING");
//        HashSet<ProductDetails> products = new HashSet<>();
//        ProductDetails product = new ProductDetails(UUID.randomUUID());
//        products.add(product);
//        product = new ProductDetails(UUID.randomUUID());
//        product.name = "Test name MQ";
//        products.add(product);
//
//
//        transmitter.sendMessage(products);
//        System.out.println("SEEEEEENT");
//
//    }
//
//    @PostMapping("/receive")
//    public void getReceive() {
//
//        System.out.println("TEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEST RECEIVING");
//        new Receiver().run();
//        System.out.println("TEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEST  RECEIVED");
//    }
}
