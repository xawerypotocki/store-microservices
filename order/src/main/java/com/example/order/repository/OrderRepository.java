package com.example.order.repository;

import com.example.order.entity.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;
import java.util.UUID;

public interface OrderRepository extends CrudRepository<Order, UUID> {
    Set<Order> findAll();
}
