package com.example.order.jms;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.io.Serializable;

@Component
public class Transmitter <T extends Serializable> implements CommunicationParameters {

    private Session session;
    private MessageProducer producer;

    public Transmitter() {
        try {
            initialize();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    private void initialize() throws JMSException {
        Connection connection = null;
        try {
            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);

            // Create a Connection
            connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination (Topic or Queue)
            Queue destination = session.createQueue(QUEUE_NAME_TX);

            // Create a MessageProducer from the Session to the Topic or Queue
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

        } catch (Exception e) {
            System.out.println("Caught: " + e);
            e.printStackTrace();
            if (session != null) {
                session.close();
                session = null;
            }
            if (connection != null) {
                connection.close();
                connection = null;
            }
        }
    }

    public void sendMessage(T o) {
        try {
            if (session == null || producer == null) {
                initialize();
            }
            ObjectMessage message = session.createObjectMessage(o);
            producer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

}
