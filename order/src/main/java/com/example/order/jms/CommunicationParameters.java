package com.example.order.jms;

public interface CommunicationParameters {
    String QUEUE_NAME_TX = "orderQueueToProd";
    String QUEUE_NAME_RX = "orderQueueFromProd";

    String BROKER_URL = "tcp://localhost:61616";
}
