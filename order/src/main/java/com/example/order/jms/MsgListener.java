package com.example.order.jms;

import com.example.common.model.ProductDetails;
import com.example.order.service.ProductDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.util.HashSet;

@Component
public class MsgListener implements MessageListener {

    @Autowired
    private ProductDetailsService productDetailsService;

    @Override
    public void onMessage(Message message) {

        if (message instanceof ObjectMessage) {
            ObjectMessage objectMessage = (ObjectMessage) message;
            Object o = null;
            try {
                o = objectMessage.getObject();
            } catch (JMSException e) {
                e.printStackTrace();
            }
            if (o instanceof HashSet) {
                HashSet<ProductDetails> products = (HashSet) o;
                System.out.println("Received products: ");
                productDetailsService.add(products);

            }
        } else {
            System.out.println("Received: " + message);
        }
    }
}
