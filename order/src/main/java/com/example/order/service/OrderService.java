package com.example.order.service;

import com.example.order.entity.Order;
import com.example.common.model.OrderDetails;

import java.util.Set;
import java.util.UUID;

public interface OrderService {
    Set<Order> getAllOrders();
    OrderDetails getOrderWithDetails(UUID id);
}
