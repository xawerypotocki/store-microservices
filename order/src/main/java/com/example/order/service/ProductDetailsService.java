package com.example.order.service;

import com.example.common.model.ProductDetails;

import java.util.Set;
import java.util.UUID;

public interface ProductDetailsService {
    void add(Set<ProductDetails> productDetails);
    ProductDetails get(UUID id, long timeout, Object listener);
}
