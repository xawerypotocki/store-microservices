package com.example.order.service;

import com.example.order.entity.Order;
import com.example.order.jms.Transmitter;
import com.example.common.model.OrderDetails;
import com.example.common.model.ProductDetails;
import com.example.order.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderService {

    public static final int TIMEOUT_PRODUCT_SERVICE = 10000; // in ms

    private final OrderRepository orderRepository;
    private Transmitter<HashSet<ProductDetails>> transmitter;
    private ProductDetailsService productDetailsService;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, Transmitter<HashSet<ProductDetails>> transmitter,ProductDetailsService productDetailsService) {
        this.orderRepository = orderRepository;
        this.transmitter = transmitter;
        this.productDetailsService = productDetailsService;
    }

    @Override
    public Set<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public OrderDetails getOrderWithDetails(UUID id) {
        Optional<Order> order = orderRepository.findById(id);
        OrderDetails orderDetails = new OrderDetails();
        order.ifPresentOrElse(o -> orderDetails.mapFromOrder(o), () -> {throw new IllegalArgumentException("No such order.");});

        orderDetails.customerName = "Mocked name - can be implemented as product names"; // here the next
        // MQ queue can be used (as for order details) to obtain dara from customer service
        obtainProductDetails(orderDetails.products);
        return orderDetails;
    }

    private void obtainProductDetails(Set<ProductDetails> products) {
        if (products instanceof HashSet) {
            transmitter.sendMessage((HashSet<ProductDetails>) products);
        }
        products.stream().forEach(p -> p.name = productDetailsService.get(p.id,TIMEOUT_PRODUCT_SERVICE, this).getName());
    }
}
