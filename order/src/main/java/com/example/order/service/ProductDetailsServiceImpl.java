package com.example.order.service;

import com.example.common.model.ProductDetails;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class ProductDetailsServiceImpl implements ProductDetailsService {
    private Map<UUID, ProductDetails> productsMap = new ConcurrentHashMap<>();
    private Map<UUID, Object> awaitingIds = new HashMap<>();

    public void add(Set<ProductDetails> products) {
        productsMap.putAll(products.stream().collect(Collectors.toMap(p -> p.id, p -> p)));
        notifyListeners();
    }

     public ProductDetails get(UUID id, long timeout, Object listener) {
         ProductDetails productDetails = readDataIfExists(id);

         if (productDetails != null) {
             return productDetails;
         }
         awaitingIds.put(id, listener);

         try {
             synchronized (listener) {
                 listener.wait(timeout);
             }
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
         productDetails = readDataIfExists(id);

         if (productDetails == null) {
             productDetails = new ProductDetails(id);
         }
         return productDetails;
    }

    private ProductDetails readDataIfExists(UUID id) {
        ProductDetails product = null;
        if (productsMap.containsKey(id)) {
            product = productsMap.get(id);
            productsMap.remove(id);
        }
        return product;
    }

    private void notifyListeners() {
        for(UUID id : awaitingIds.keySet()) {
            if(productsMap.containsKey(id)) {
                Object monitor = awaitingIds.get(id);
                synchronized (monitor) {
                    monitor.notify();
                }
            }
        }
    }
}
