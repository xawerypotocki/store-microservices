package com.example.common.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class ProductDetails implements Serializable {

    public UUID id;
    public String name;

    public ProductDetails(UUID id) {
        this.id = id;
    }

    public static ProductDetails fromProduct(UUID id) {
        return new ProductDetails(id);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDetails that = (ProductDetails) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
