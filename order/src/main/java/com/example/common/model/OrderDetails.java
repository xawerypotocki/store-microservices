package com.example.common.model;

import com.example.order.entity.Order;

import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class OrderDetails {
    public UUID id;
    public Date date;
    public Set<ProductDetails> products;
    public Integer totalQty;
    public Float totalPrice;
    public UUID customerId;
    public String customerName;

    public void mapFromOrder(Order order) {
        id = order.getId();
        date = order.getDate();
        totalQty = order.getTotalQty();
        totalPrice = order.getTotalPrice();
        customerId = order.getCustomerId();
        products = order.getProducts().stream().map(ProductDetails::fromProduct).collect(Collectors.toSet());
    }
}
