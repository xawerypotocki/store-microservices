insert into customer values ('8eedfd3a367d11ebadc10242ac130000', 'Address 0', 'Name 0');
insert into customer values ('8eedfd3a367d11ebadc10242ac130001', 'Address 1', 'Name 1');
insert into customer values ('8eedfd3a367d11ebadc10242ac130002', 'Address 2', 'Name 2');
insert into customer values ('8eedfd3a367d11ebadc10242ac130003', 'Address 3', 'Name 3');
insert into customer values ('8eedfd3a367d11ebadc10242ac130004', 'Address 4', 'Name 4');


insert into order_history values (1, '8eedfd3a367d11ebadc10242ac130000', '8eedfd3a367d11ebadc10242ac170000', 'Delivered');
insert into order_history values (2, '8eedfd3a367d11ebadc10242ac130000', '8eedfd3a367d11ebadc10242ac170001', 'Prepared');
insert into order_history values (3, '8eedfd3a367d11ebadc10242ac130000', '8eedfd3a367d11ebadc10242ac170002', 'Delivered');
insert into order_history values (4, '8eedfd3a367d11ebadc10242ac130000', '8eedfd3a367d11ebadc10242ac170003', 'Delivered');
insert into order_history values (5, '8eedfd3a367d11ebadc10242ac130000', '8eedfd3a367d11ebadc10242ac170004', 'Delivered');
insert into order_history values (6, '8eedfd3a367d11ebadc10242ac130000', '8eedfd3a367d11ebadc10242ac170005', 'Delivered');