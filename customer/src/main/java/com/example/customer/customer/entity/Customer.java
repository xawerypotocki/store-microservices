package com.example.customer.customer.entity;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="customer")
public class Customer {
    @Id
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
   private String address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="customer_id")
    private Set<OrderHistory> orderHistory;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<OrderHistory> getOrderHistory() {
        return orderHistory;
    }

    public void setOrderHistory(Set<OrderHistory> orderHistory) {
        this.orderHistory = orderHistory;
    }
}
