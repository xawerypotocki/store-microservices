package com.example.customer.customer.entity;


import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="order_history")
public class OrderHistory {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @JoinColumn(name="customer_id", nullable=false)
    private UUID customer_id;

    @Column(name = "order_id")
    private UUID orderId;

    @Column(name = "order_status")
    private String orderStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(UUID customer_id) {
        this.customer_id = customer_id;
    }

    public UUID getOrderId() {
        return orderId;
    }

    public void setOrderId(UUID orderId) {
        this.orderId = orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

}
