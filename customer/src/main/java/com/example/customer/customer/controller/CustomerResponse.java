package com.example.customer.customer.controller;

import com.example.customer.customer.entity.Customer;

import java.util.UUID;

public class CustomerResponse {
    public UUID id;
    public String name;
    public String address;

    public static CustomerResponse fromCustomer(Customer customer) {
        CustomerResponse response = new CustomerResponse();
        response.id = customer.getId();
        response.name = customer.getName();
        response.address = customer.getAddress();
        return response;
    }
}
