package com.example.customer.customer.controller;

import com.example.customer.customer.entity.OrderHistory;
import com.example.customer.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/customers")
    public List<CustomerResponse> getAllCustomers() {
        return customerService.getAllCustomers().stream().map(CustomerResponse::fromCustomer).collect(Collectors.toList());
    }

    @GetMapping("/customers/{id}")
    public Set<OrderHistory> getOrdersHistoryOfCustomer(@PathVariable String id) {
        return customerService.getOrdersHistoryOfCustomer(UUID.fromString(id));
    }
}
