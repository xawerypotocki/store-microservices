package com.example.customer.customer.repository;

import com.example.customer.customer.entity.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface CustomerRepository extends CrudRepository<Customer, UUID> {

    List<Customer> findAll();
    Customer findCustomerById(UUID id);
}
