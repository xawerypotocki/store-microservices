package com.example.customer.customer.service;

import com.example.customer.customer.entity.Customer;
import com.example.customer.customer.entity.OrderHistory;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface CustomerService {
    List<Customer> getAllCustomers();

    Set<OrderHistory> getOrdersHistoryOfCustomer(UUID id);
}