package com.example.customer.customer.service;

import com.example.customer.customer.entity.Customer;
import com.example.customer.customer.entity.OrderHistory;
import com.example.customer.customer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomers() {
        List<Customer> test  = customerRepository.findAll();
        return customerRepository.findAll();
    }

    @Override
    public Set<OrderHistory> getOrdersHistoryOfCustomer(UUID id) {
        Customer customer = customerRepository.findCustomerById(id);
        Set<OrderHistory> history = customer.getOrderHistory();
        return history;
    }
}
